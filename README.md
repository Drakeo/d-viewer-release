Second Life Viewer
====================
This project manages the source code for the
[Second Life](https://www.secondlife.com) Viewer.

This source is available as open source; for details on licensing, see
# README #
# CEF Media Plugin  With Dullahan Host Linux viewer. 
Liden Labs no longer supports the linux viewer so I have done my best to keep up.
This is a copy of the linux Linden labs viewer with the latest media. 
# Compiled  Slackware 14.2  gcc 5.5.0 GLIBC 2.23 
enjoy. 
CEF branch 3626.1895 Chromium: 72.0.3626.121
[the licensing page on the Second Life wiki](https://wiki.secondlife.com/wiki/Linden_Lab_Official:Second_Life_Viewer_Licensing_Program)

For information on how to use and contribute to this, see
[the open source portal on the wiki](https://wiki.secondlife.com/wiki/Open_Source_Portal).

To download the current default version, visit
[the download page](https://secondlife.com/support/downloads). For
even newer versions try
[the Alternate Viewers page](https://wiki.secondlife.com/wiki/Linden_Lab_Official:Alternate_Viewers)
